package minecade.dungeonrealms;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CommandCheckFirstSlot implements CommandExecutor {


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		ItemStack item = p.getItemInHand();
		Main.getLog().info("type: "+item.getType());
		Main.getLog().info("dura: "+item.getDurability());
		ItemMeta mata = item.getItemMeta();
		Main.getLog().info("metadata: ");
		Main.getLog().info("name: "+mata.getDisplayName());
		Main.getLog().info("lore: "+mata.getLore());
		return true;
	}
	
	
}
