package minecade.dungeonrealms.TeleportationMechanics.commands;

import minecade.dungeonrealms.TeleportationMechanics.City;
import minecade.dungeonrealms.TeleportationMechanics.TeleportationMechanics;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandDRTP implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if(!(p.isOp())) { return true; }
		for(ItemStack scroll: City.getScrolls()){
				p.getInventory().addItem(CraftItemStack.asCraftCopy(scroll));
		}
		return true;
	}
	
}