package minecade.dungeonrealms.TeleportationMechanics;

import minecade.dungeonrealms.ItemMechanics.ItemMechanics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public enum City {
    Navara((short) 1,new Location(Bukkit.getWorlds().get(0), -149, 85, 765, 92.0F, 1F),"Navara"),
    Thyran((short) 2,new Location(Bukkit.getWorlds().get(0), 388, 99, -449, 2.0F, 1F),"Thyran"),
    DeadPeaks((short) 3,new Location(Bukkit.getWorlds().get(0), 388, 99, -449, 2.0F, 1F),"DeadPeaks") ;

    private final Location location;
    private final String name;
    private short id;
    private ItemStack scroll;

    City(short id,Location loc,String name){
        this.id=id;
        this.location=loc;
        this.name=name;
        scroll = ItemMechanics.signNewCustomItem(
                Material.BOOK, id,
                ChatColor.WHITE.toString() + "" + ChatColor.BOLD.toString() + "Teleport:" + ChatColor.WHITE.toString() + " " + name,
                ChatColor.GRAY.toString() + "Teleports the user to " + name + ".");
        add(id,this);
    }
    private static Map<Short, City> map;
    private static int size=0;
    private static List<ItemStack> scrolls;
    private static void add(short id,City city){
        if(map==null)
            map=new HashMap<>();
        if(scrolls==null)
            scrolls=new ArrayList<ItemStack>();
        map.put(id,city);
        scrolls.add(city.getScroll());
        size++;
    }
    public short getId(){return id;}
    public Location getLocation(){return location;}
    public String getName(){return name;}
    public ItemStack getScroll(){return scroll;}
    public static City getCity(short id){return map.get(id);}

    public static City getRandomCity(){
        int random = new Random().nextInt(size);
        return map.values().toArray(new City[0])[random];
    }
    public static ItemStack getRandomScroll(){
        return getRandomCity().getScroll();
    }
    public static ItemStack[] getScrolls(){
        return scrolls.toArray(new ItemStack[0]);
    }


}
