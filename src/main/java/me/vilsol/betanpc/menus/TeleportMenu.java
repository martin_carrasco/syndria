package me.vilsol.betanpc.menus;

import org.bukkit.ChatColor;

import me.vilsol.betanpc.items.teleportmenu.TeleportToNavara;
import me.vilsol.betanpc.items.teleportmenu.TeleportToThyran;
import me.vilsol.betanpc.items.utils.BackToMainMenu;
import me.vilsol.menuengine.engine.MenuModel;

public class TeleportMenu extends MenuModel {

	public TeleportMenu() {
		super(9, ChatColor.GOLD.toString() + ChatColor.BOLD + "Spawn Teleport Books");
		getMenu().addItem(TeleportToNavara.class, 0);
		getMenu().addItem(TeleportToThyran.class, 1);
		getMenu().addItem(BackToMainMenu.class, 2);
	}
	
}
